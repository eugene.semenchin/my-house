var swiper = new Swiper(".swiper", {
    slidesPerView: 1,
    spaceBetween: 20,
    loop: true,
    freeMode: true,
    autoplay: true,
    speed: 2000,
    breakpoints: {
        576: {
            slidesPerView: 2,
        },

        769: {
            slidesPerView: 3,
        },
    },

    navigation: {
        nextEl: '.swiper-next',
        prevEl: '.swiper-prev',
      },
});