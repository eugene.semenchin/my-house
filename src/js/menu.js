const burgerBtn = document.querySelector('.burger__btn');
const navMenu = document.querySelector('.navigation');
const body = document.querySelector('body');

burgerBtn.addEventListener('click', () => {
    burgerBtn.classList.toggle('active');
    navMenu.classList.toggle('active');
    body.classList.toggle('no-scroll')
})

